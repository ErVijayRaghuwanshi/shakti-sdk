

#include "platform.h"
#include "gpio.h"
#include "utils.h"

/** @fn void main()
 * @brief Correspondingly making GPIO0 as HIGH and LOW.
 * @details It will make sound when you give HIGH signal
 */
void main()
{
    //Assumption 1 ---> output, 0 ---> input
    write_word(GPIO_DIRECTION_CNTRL_REG, GPIO0);

    while (1) {
        write_word(GPIO_DATA_REG, 0xff);

        delay_loop(1000, 1000);

        write_word(GPIO_DATA_REG, GPIO0);
        write_word(GPIO_DATA_REG, GPIO1);
        write_word(GPIO_DATA_REG, GPIO2);
        write_word(GPIO_DATA_REG, GPIO3);
        write_word(GPIO_DATA_REG, GPIO4);
        write_word(GPIO_DATA_REG, GPIO5);
        write_word(GPIO_DATA_REG, GPIO6);
        write_word(GPIO_DATA_REG, GPIO7);
        write_word(GPIO_DATA_REG, GPIO8);
        write_word(GPIO_DATA_REG, GPIO9);
        write_word(GPIO_DATA_REG, GPIO10);
        write_word(GPIO_DATA_REG, GPIO11);
        write_word(GPIO_DATA_REG, GPIO12);
        write_word(GPIO_DATA_REG, GPIO13);
        write_word(GPIO_DATA_REG, GPIO14);
        write_word(GPIO_DATA_REG, GPIO15);
        write_word(GPIO_DATA_REG, GPIO16);
        write_word(GPIO_DATA_REG, GPIO17);
        write_word(GPIO_DATA_REG, GPIO18);

        delay_loop(1000,1000);

        printf("\nWORKED SUCCESSFULLY");
    }
}
